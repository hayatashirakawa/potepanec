module Potepan::ProductDecorator
  def related_products(limit)
    Spree::Product.in_taxons(taxons).where.not(id: id).distinct.limit(limit)
  end
  Spree::Product.prepend self
end
