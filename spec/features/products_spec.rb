RSpec.feature "Products", type: :feature do
  describe "商品詳細ページに関するテスト" do
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:taxon) { create(:taxon) }
    let!(:related_products) { create_list(:product, Const::RELATED_PRODUCTS_MAX + 1, taxons: [taxon]) }

    before do
      visit potepan_product_path(product.id)
    end

    context "商品詳細ページで表示されている内容" do
      it "カートへ入れるのリンクが表示されていること" do
        expect(page).to have_selector "a", text: 'カートへ入れる'
      end

      it "HOMEへのリンクがあること" do
        expect(page).to have_link "Home"
      end

      it "商品名が表示されていること" do
        expect(page).to have_content product.name
      end

      it "商品の値段が表示されていること" do
        expect(page).to have_content product.display_price
      end

      it "商品説明が表示されていること" do
        expect(page).to have_content product.description
      end

      it "関連商品がRELATED_PRODUCTS_MAXと等しい数表示されていること" do
        expect(page).to have_css ".productBox", count: Const::RELATED_PRODUCTS_MAX
      end

      it "関連商品に関する表示がされていること" do
        product.related_products(Const::RELATED_PRODUCTS_MAX).each do |product|
          expect(page).to have_content product.name
          expect(page).to have_content product.display_price
        end
      end
    end

    context "関連商品が3つの場合" do
      let!(:related_products) { create_list(:product, 3, taxons: [taxon]) }

      it "3つまで表示されていること" do
        expect(page).to have_css ".productBox", count: 3
      end
    end

    context "それぞれのリンクをクリックした場合" do
      it "トップページに遷移すること" do
        click_link "HOME"
        expect(current_path).to eq potepan_index_path
      end

      it "一覧ページへ戻ること" do
        click_link  "一覧ページへ戻る"
        expect(current_path).to eq potepan_category_path(taxon.id)
      end

      it "関連商品の詳細ページへ遷移すること" do
        product.related_products(Const::RELATED_PRODUCTS_MAX).each do |product|
          click_on product.name
          expect(current_path).to eq potepan_product_path(product.id)
        end
      end
    end
  end
end
