RSpec.feature "Categories", type: :feature do
  let(:categories)   { create(:categories) }
  let(:taxon)        { create(:taxon, name: "Foobar", parent: taxonomy.root, taxonomy: taxonomy) }
  let(:other_taxon)  { create(:taxon, name: "Other",  parent: taxonomy.root, taxonomy: taxonomy) }
  let(:taxonomy)     { create(:taxonomy) }
  let!(:product_1)   { create(:product, name: "Foobar-Bag", price: 120, taxons: [taxon]) }
  let!(:product_2)   { create(:product, name: "Foobar-Mug", price: 150, taxons: [taxon]) }
  let!(:product_3)   { create(:product, name: "Other-Shirts", taxons: [other_taxon]) }

  before do
    visit potepan_category_path(taxon.id)
  end

  context "カテゴリページを見た場合" do
    it "商品名が表示されていること" do
      expect(page).to have_content "Foobar-Bag"
      expect(page).to have_content "Foobar-Mug"
    end

    it "商品の値段が表示されていること" do
      expect(page).to have_content 120
      expect(page).to have_content 150
    end

    it "他のブランドの商品名は表示されていないこと" do
      expect(page).not_to have_content "Other-Shirts"
    end
  end

  it "商品名をクリックした場合商品詳細ページへ遷移すること" do
    click_on product_1.name
    expect(current_path).to eq potepan_product_path(product_1.id)
    click_on '一覧ページへ戻る'
    expect(current_path).to eq potepan_category_path(product_1.taxons.first.id)
  end

  it "サイドバーにブランド(taxonomy)と商品(taxon)が表示されていること" do
    within("#taxonomy-link") do
      expect(page).to have_content 'Brand'
      expect(page).to have_content "Foobar(2)"
      expect(page).to have_content "Other(1)"
      click_on 'Foobar'
      expect(current_path).to eq potepan_category_path(taxon.id)
    end
  end
end
