RSpec.describe "Categories", type: :request do
  let(:taxon)    { create(:taxon) }
  let(:taxonomy) { create(:taxonomy) }
  let(:product)  { create(:product) }

  before do
    get potepan_category_path(taxon.id)
  end

  describe "カテゴリ画面に関するテスト" do
    it "HTTPステータス200を返すこと" do
      expect(response).to have_http_status(200)
    end

    it "カテゴリ(taxonomy)が表示されること" do
      expect(response.body).to include taxonomy.name
    end

    it "分類(taxon)が表示されること" do
      expect(response.body).to include taxon.name
    end

    it "分類(taxon)の商品数が表示されること" do
      expect(response.body).to include taxon.products.count.to_s
    end

    it "正しいviewが表示されていること" do
      expect(response).to render_template :show
    end
  end
end
