RSpec.describe "Products", type: :request do
  describe "商品詳細画面のテスト" do
    let(:product) { create(:product) }
    let(:taxon)   { create(:taxon) }
    let!(:related_product) { create(:product, taxons: [taxon]) }

    before do
      get potepan_product_path(product.id)
    end

    it "HTTPステータス200を返すこと" do
      expect(response).to have_http_status(200)
    end

    it "商品名が表示されていること" do
      expect(response.body).to include product.name
    end

    it "商品の値段が表示されていること" do
      expect(response.body).to include product.display_price.to_s
    end

    it "商品詳細が表示されていること" do
      expect(response.body).to include product.description
    end

    it "正しいviewが表示されていること" do
      expect(response).to render_template :show
    end

    it "各パーシャルでまとめたテンプレートがレンダリングされていること" do
      expect(response).to render_template :application
    end

    it "関連商品が表示されていること" do
      expect(response.body).to include related_product.name
      expect(response.body).to include related_product.display_price.to_s
    end
  end
end
