RSpec.describe ApplicationHelper, type: :helper do
  context "ページタイトルがある場合" do
    it "full_titleが表示されていること" do
      expect(full_title("title")).to eq "title | BIG BAG"
    end
  end

  context "ページタイトルがない場合" do
    it "空白の場合BIG BAGのみを返すこと" do
      expect(full_title(" ")).to eq "BIG BAG"
    end

    it "ページタイトルがない場合BIG BAGのみを返すこと" do
      expect(full_title("")).to eq "BIG BAG"
    end
  end

  context "ページタイトルにnilが渡ってきた場合" do
    it "BIG BAGのみを返すこと" do
      expect(full_title(nil)).to eq "BIG BAG"
    end
  end
end
