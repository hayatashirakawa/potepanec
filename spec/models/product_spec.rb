RSpec.describe "Product", type: :model do
  let!(:product)      { create(:product) }
  let!(:taxon)        { create(:taxon) }
  let(:taxonomy)      { create(:taxonomy) }
  let!(:related_products) { create_list(:product, Const::RELATED_PRODUCTS_MAX + 1, taxons: [taxon]) }
  let(:other_taxon)   { create(:taxon, name: "Other", parent: taxonomy.root, taxonomy: taxonomy) }
  let(:other_product) { create(:product, name: "Other-Shirts", taxons: [other_taxon]) }

  context "関連商品がRELATED_PRODUCTS_MAX + 1個の場合" do
    it "関連商品がRELATED_PRODUCTS_MAXと等しい数までしか表示されないこと" do
      expect(product.related_products(Const::RELATED_PRODUCTS_MAX).length).to eq Const::RELATED_PRODUCTS_MAX
    end

    it "商品詳細ページの主商品のIDは含まれないないこと" do
      expect(product.related_products(Const::RELATED_PRODUCTS_MAX).ids).not_to include product.id
    end

    it "関連しない商品は含まれないこと" do
      expect(product.related_products(Const::RELATED_PRODUCTS_MAX)).not_to include other_product
    end
  end
end
